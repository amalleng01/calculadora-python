# coding=utf-8
import time, os

def main():
    os.system("clear")
    print "\t Calculadora"
    print "\t\t1. Sumar"
    print "\t\t2. Restar"
    print "\t\t3. Multiplicar"
    print "\t\t4. Dividir"
    print "\t\t0. Salir"

    try:
        opcion=int(input("Elija opción: "))

        if opcion == 0:
            exit()

        n1=int(input("Escriba el primer número: "))
        n2=int(input("Escriba el segundo número: "))

        if opcion == 1:
            print "El resultado es: ", n1+n2
            raw_input("\tPresione una tecla para continuar...")
            main()

        elif opcion == 2:
            print "El resultado es: ", n1-n2
            raw_input("\tPresione una tecla para continuar...")
            main()

        elif opcion == 3:
            print "El resultado es: ", n1*n2
            raw_input("\tPresione una tecla para continuar...")
            main()

        elif opcion == 4:
            if (n2==0):
                print "No se puede dividir entre 0"
                raw_input("\tPresione una tecla para continuar...")
            else:
                print "El resultado es: ", n1/n2
                raw_input("\tPresione una tecla para continuar...")
                main()
        else:
            print "Error!"
            time.sleep(0.5)
            main()

    except NameError:
        print("Error!")
        time.sleep(0.5)
        main()

if __name__ == '__main__':
    main()
